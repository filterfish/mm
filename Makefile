build:
	go build


mm: build

clean:
	rm -f mm

package-clean:
	rm debian/mm.substvars debian/mm.debhelper.log

package:
	dpkg-buildpackage --build=binary -us -uc

.PHONY: build
